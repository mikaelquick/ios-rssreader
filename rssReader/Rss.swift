//
//  Rss.swift
//  rssReader
//
//  Created by Mikael  Quick on 2017-09-07.
//  Copyright © 2017 MikaelQuick. All rights reserved.
//

import Foundation

class RSS{
    
    var title : String?
    var link : String?
    var picture : String?
}
