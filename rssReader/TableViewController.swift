//
//  TableViewController.swift
//  rssReader
//
//  Created by Mikael  Quick on 2017-09-04.
//  Copyright © 2017 MikaelQuick. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController , XMLParserDelegate{

    @IBOutlet weak var logoText: UILabel!
    
    var myRSSFeed : [RSS] = []
    var tempSave : String!
    var currentPage : String!
    var singelNews = RSS()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        parseRSS(urlToParse: "https://www.nasa.gov/rss/dyn/breaking_news.rss")
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myRSSFeed.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //Creating & styling the cell
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = myRSSFeed[indexPath.row].title
        cell.textLabel?.textColor = UIColor.white
        
        //Tapping
        cell.isUserInteractionEnabled = true
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        cell.addGestureRecognizer(tapGestureRecognizer)
        cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 25)
        cell.tag = indexPath.row
        
        //Setting the Image
        let yourImageView = cell.viewWithTag(666) as! UIImageView
        link2Image(imageView: yourImageView, row: indexPath.row)

        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let webView = segue.destination as! WebViewController
        webView.currentPage = currentPage
    }

    
    func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let number = tapGestureRecognizer.view?.tag
        currentPage = myRSSFeed[number!].link!
        performSegue(withIdentifier: "yes", sender: self)
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String){
        self.tempSave = string
    }
    
    func parser(_ parser: XMLParser, didStartElement element: String, namespaceURI: String?, qualifiedName: String?, attributes: [String : String]){
        
        if element == "title"{
            singelNews = RSS()
            
        }
        if element == "enclosure"{
            singelNews.picture = attributes["url"]!
        }
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?){
        
        if elementName == "title"{
            singelNews.title = tempSave
        }
        
        if elementName == "guid"{
            singelNews.link = tempSave
        }
        
        if elementName == "item"{
            myRSSFeed.append(singelNews)
        }
    }
    
    func parseRSS(urlToParse : String){
        let urlToSend: URL = URL(string: urlToParse)!
        let parser = XMLParser(contentsOf: urlToSend)!
        parser.delegate = self
        parser.parse()
    }
    
    func link2Image(imageView : UIImageView, row : Int){
        let url = NSURL(string: myRSSFeed[row].picture!)
        let data1 = NSData(contentsOf:url! as URL)
        imageView.image = UIImage(data: data1! as Data)
    }
}
